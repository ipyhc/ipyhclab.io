# Highcharts

This page is a brief introduction to [Highcharts](https://www.highcharts.com/).  
See the [official documentation](https://www.highcharts.com/docs/)
for exhaustive information.

## Introduction

[Highcharts](https://www.highcharts.com/) is a popular, flexible, user-friendly JavaScript graph visualization library. The library can render several types of data, from charts to timelines through maps. We focus on the two most popular products:
+ [Highcharts](https://www.highcharts.com/blog/products/highcharts/): interactive charts
+ [Highstock](https://www.highcharts.com/blog/products/highstock/): stock charts and timelines

Checkout their [demo page](https://www.highcharts.com/demo) and experiment.

## Basics

The Highchart charts have only one constructor `chart` with three parameters (and only one non-optional):

+ the DOM element `renderTo` in which the chart is to be located, or its id (optional)
+ the `options` containing user data and chart configuration
+ the `callback` function (optional)

<!-- After the chart is built, the API is accessible as a property of `options`. It enables modifying the chart, accessing data, etc. -->


Create a chart using the id of a DOM element:
```HTML
<div id="container" style="width:100%; height:400px;"></div>
```

```javascript
var myChart = Highcharts.chart('container', chartOptions);
```

### Simple chart

Here is the example of a very simple chart.

Code:

```javascript
// Custom parameters and data
var chartOptions = {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Fruit Consumption'
        },
        xAxis: {
            categories: ['Apples', 'Bananas', 'Oranges']
        },
        yAxis: {
            title: {
                text: 'Fruit eaten'
            }
        },
        series: [{
            name: 'Jane',
            data: [1, 0, 4]
        }, {
            name: 'John',
            data: [5, 7, 3]
        }]
    }

// Use an existing DOM element
var chartDiv = document.querySelector("#myChart");
var myChart = Highcharts.chart(chartDiv, chartOptions);
```

<demo1/>

<!-- TODO: insert Vue rendered example -->

### Simple stock chart

There is a seperate constructor method to create a stock chart, called `stockChart`. This constructor works similarly to `chart`.

The data is typically stored in a predefined JavaScript array.

```javascript
var myData = [[1496151000000,153.67],[1496237400000,152.76],[1496323800000,153.18],[1496410200000,155.45],[1496669400000,153.93],[1496755800000,154.45],[1496842200000,155.37],[1496928600000,154.99],[1497015000000,148.98],[1497274200000,145.42],[1497360600000,146.59],[1497447000000,145.16],[1497533400000,144.29],[1497619800000,142.27],[1497879000000,146.34],[1497965400000,145.01],[1498051800000,145.87],[1498138200000,145.63],[1498224600000,146.28],[1498483800000,145.82],[1498570200000,143.73],[1498656600000,145.83],[1498743000000,143.68],[1498829400000,144.02],[1499088600000,143.5],[1499261400000,144.09],[1499347800000,142.73],[1499434200000,144.18],[1499693400000,145.06],[1499779800000,145.53]];

var myStockChart = Highcharts.stockChart('container', {
        rangeSelector: {
            selected: 1
        },

        title: {
            text: 'AAPL Stock Price'
        },

        series: [{
            name: 'AAPL',
            data: myData, // predefined JavaScript array
            tooltip: {
                valueDecimals: 2
            }
        }]
    });
```

<example-basic-chart/>

<!-- TODO: Some advanced features? -->