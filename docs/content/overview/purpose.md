# Purpose

## Why?

The goal of this [ipywidget](https://ipywidgets.readthedocs.io/en/latest/) is to
make Jupyter notebook users able to run the powerful JavaScript library 
[Highcharts](https://www.highcharts.com/), for both Highcharts and Highstock.

This widget is the direct successor of [ezhc](https://github.com/oscar6echo/ezhc).

## How?

This library essentially is a custom Jupyter [ipywidget](https://ipywidgets.readthedocs.io/en/latest).

For more info see the [ipywigets official documentation](https://ipywidgets.readthedocs.io/en/latest/examples/Widget%20Custom.html)
and maybe the article [Authoring Custom Jupyter Widgets](https://blog.jupyter.org/authoring-custom-jupyter-widgets-2884a462e724).

Like all ipywidgets, **ipyhc** has 2 parts:

- The **Python** part: It validates the user input and prepares and passes it to
  the JavaScript part. It also allows the configuration of the type
  interactivity the user wants from the chart.
- The **Javascript** part: From the user data and configuration it builds the 
  chart and sets up the interactivity required. After some manipulation it enables 
  the user to synchronize the data back to the Python kernel.  

**ipyhc** is:
+ designed to enable easy access to basic Highcharts features but
  allows unlimited customization through a full access to the Highcharts options 
+ as transparent as possible to give flexible and unhindered access to the
  underlying library
