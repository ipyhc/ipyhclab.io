---
home: true
description: 'Highcharts in Jupyter notebooks'
heroImage: '/ipyhc_logo.svg'
heroText: 'ipyhc'
actionText: Get Started
actionLink: /guide/install
features:
- title: Accessible
  details: Display your data as interactive charts, stock charts and timelines.
- title: Transparent
  details: Access all Highcharts features with a direct access to the configuration options.
- title: Customizable
  details: Create your own Javascript functions to interact with the chart. Leverage examples in the Highcharts docs.
footer: MIT Licensed | Copyright © 2019-present Louis Raison, Pierre Tholoniat, Olivier Borderies.
pageClass: custom-home-page
---

### Install

```bash
# for notebook >= 5.3
$ pip install ipyhc
```

::: tip COMPATIBILITY NOTE
Tested on Chrome and Firefox.
:::
