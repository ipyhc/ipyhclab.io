
# Structure

The architecture of *ipyhc* is similar to the one of *ipyaggrid*, for which the technical decisions are described on this [documentation page](https://dgothrek.gitlab.io/ipyaggrid/dev/structure.html).