# Dev Install

This section is intentionally verbose in order to help ipywidget developer in the Classic notebook.
For more info about authoring Jupyter custom ipywidgets, see this [article](https://blog.jupyter.org/authoring-custom-jupyter-widgets-2884a462e724).

## Setup

Download the source:

```bash
$ git clone https://gitlab.com/ipyhc/ipyhc

$ cd ipyhc
```

It is tidier hence recommended to work in an isolated environment, for instance with [Miniconda](https://docs.conda.io/en/latest/miniconda.html).
Create a dedicated environment:

```bash
# create conda env
$ conda create -n dev-ipyhc python=3
# activate env (without source for Windows)
$ source activate dev-ipyhc
# install dependencies
(dev-ipyhc) $ pip install notebook
(dev-ipyhc) $ pip install -r requirements.txt
```

The commands below are assumed to run from this `dev-ipyhc` environment.

## Folders

It is instructing to keep an eye on configuration folders to follow what the install modifies in the file system.

```bash
# list jupyter folders
$ jupyter --paths
# you should see something like:
config:
    /home/pierre/.jupyter # user
    /home/pierre/.miniconda/envs/dev-ipyhc/etc/jupyter # sys-prefix
    /usr/local/etc/jupyter # system
    /etc/jupyter # default
data:
    /home/pierre/.local/share/jupyter # user
    /home/pierre/.miniconda/envs/dev-ipyhc/share/jupyter # sys-prefix
    /usr/local/share/jupyter # system
    /usr/share/jupyter # default
runtime:
    /run/user/1000/jupyter
```


## Javascript

Start by building the Javascript modules.  
Build the Javascript part of the ipywidget:

```bash
# install ipywidget js package
$ cd js
$ npm install

# build js
$ npm run prepare

# (optional) auto rebuild js/ upon change - run in new terminal
$ npm run watch
```

## Python

Install the Python package in [dev mode](https://packaging.python.org/tutorials/installing-packages/#installing-from-a-local-src-tree).

```bash
# install Python package
$ cd ..

$ pip install -e .
```

## Extensions

Install the notebook extension:

```bash
$ jupyter nbextension install --py --symlink --sys-prefix ipyhc
# if the target files already exist it will fail: remove them manually
# you should see something like:
Installing /home/pierre/Documents/ipyhc-dev/ipyhc/static -> ipyhc
- Validating: OK

    To initialize this nbextension in the browser every time the notebook (or other app) loads:
    
          jupyter nbextension enable ipyhc --py --sys-prefix

```

Enable the notebook extension:

```bash
$ jupyter nbextension enable --py --sys-prefix ipyhc
# you should see something like:
Enabling notebook extension ipyhc/extension...
      - Validating: OK
```

## Check Extensions

Check the notebook extension:

```bash
# notebook extensions status
$ jupyter nbextension list
# for the sys-prefix config folder you should see at least:
Known nbextensions:
  config dir: /home/pierre/.miniconda/envs/dev-ipyhc/etc/jupyter/nbconfig
    notebook section
      jupyter-js-widgets/extension  enabled 
      - Validating: OK
      ipyhc/extension  enabled 
      - Validating: OK
```

Open the notebook extension config file:

```bash
$ cat /home/pierre/.miniconda/envs/dev-ipyhc/etc/jupyter/nbconfig/notebook.json
# you should see at least something like:
{
  "load_extensions": {
    "ipyhc/extension": true
  }
}
```

Finally the nbextensions data folder should contain symlinks to the ipyhc static/ folder:

```bash
# (if necessary: brew install tree)
$ tree /home/pierre/.miniconda/envs/dev-ipyhc/share/jupyter/nbextensions
# you should see at least:
/home/pierre/.miniconda/envs/dev-ipyhc/share/jupyter/nbextensions
├── ipyhc -> /home/pierre/Documents/ipyhc-dev/ipyhc/static
└── jupyter-js-widgets
    ├── extension.js
    └── extension.js.map

```

## Run

Move the the notebooks folder and launch the notebook server:

```bash
# from repo top folder
$ cd test-zone/notebooks

# launch notebook server and open browser to the classic file system
$ jupyter notebook
```

Then open a demo notebook.

## Uninstall

Proceed in the install reverse order.

1.  Remove the frontend extension:

For the classic notebook remove the notebook extension:

```bash
$ jupyter nbextension uninstall --py --sys-prefix ipyhc
```

2.  Uninstall the Python package:

```bash
$ pip uninstall ipyhc
```
