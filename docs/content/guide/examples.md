---
sidebarDepth: 2
---

# Examples

Many of these examples are derived from the ezhc library. More examples will be added as the package development carries on.

::: tip
At any moment, if you want to know how to create an ipyhc like the doc examples, search the example you need in this interactive notebook:

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/ipyhc%2Fipyhc.gitlab.io/master?filepath=docs%2Fnotebooks)
:::

## Time series

This example shows a simple HighStock time series, with a custom tooltip.

<<< @/content/code-snippets/simple-time-series.python

<example-simple-time-series/>

## Working with DataFrames

The underlying functions enable to format the *pandas* `Dataframe` to fit to the required formats.
Thus the `Dataframe` format enables a lot of convenient features.

<<< @/content/code-snippets/computation-time-series.python

<example-computation-time-series/>

## Bar Chart

All sorts of bar charts are available with HighCharts, for more details see their [documentation](https://www.highcharts.com/docs/).

<<< @/content/code-snippets/column-bar.python

<example-column-bar/>

## Pie Chart

Pie charts are also available in ipyhc, as they are in HighCharts.

<<< @/content/code-snippets/pie-chart.python

<example-pie-chart/>

## Scatter Chart

<<< @/content/code-snippets/scatter-chart.python

<example-scatter-chart/>