---
sidebarDepth: 2
---

# Create

To create a chart with **ipyhc** you need:

- bring you data as (1) a list, (2) a numpy array or (3) a dataframe
- define highcharts `chartOptions` as a dict - see [Highcharts doc](https://www.highcharts.com/docs/)
- pass them to the `Chart` constructor, along with some options - see below
- display the ipywidget thus created

This page describes the Python parameters of *ipyhc* and their configuration options.
To see examples of graphs constructed with ipyhc, please browse the [Examples](./examples.html) section.

## Sample Setup

The params of the `Chart` constructor must have the following types:

```python
# Widget options
width : int or str,
height : int,
theme : str,

# Chart setup
stock: bool,
data : list, numpy array or pandas dataframe,
options : dict,

# Customization options
unsync: bool,
```

The default parameters are the following:

```python
def __init__(self,
             width='100%',
             height=0,
             theme='',
             stock=False,
             options={},
             data=[],
             unsync=False):
```

<!--TODO Change the format of the data-->

### User Data

`data: list, default=[]`

The data can only be input as a list of `Series` elements from Highcharts (for
more information see the official documentation on [Series](https://www.highcharts.com/docs/chart-concepts/series)).
This module includes a collection of functions that can be used
to create these `Series`

<!-- If data is input as a dataframe it is translated to ag-Grid format. -->

<!-- You can also use a list of rows as follows. However it is probably a
lot more convenient to input a pandas dataframe, the _de facto_ standard
Python container for tabular data. -->

Here is an example of a valid data input:

```python
data = [
  {'name': 'Track1',
   'data': [
    [0, 1.0],
    [1, 0.99],
    [2, 0.97],
    [3, 0.95],
    [4, 0.97]]
  },
  {'name': 'Track2',
   'data': [
    [0, 1.0],
    [1, 0.99],
    [2, 1.00],
    [3, 1.00],
    [4, 0.98]]
  }
]
```

### Chart Options

`options: dict, default={}`

The grid options are exactly those of Highcharts, in **full transparency**.
It should contain all the customization options for the grid, including
the JavaScript functions for custom actions like tooltip.

The Highcharts API is **_very rich and explicit and well documented_**, and
their [website](https://www.highcharts.com/)
has tons of examples that enable you to easily play with all the features. We
can only recommend to use the plunkers they provide to fine tune the right
configuration for your use case.

Here is a sample basic chart:

<<< @/content/code-snippets/basic-chart.python

And the output:

<example-basic-chart/>

## Parameters

This section describes the **ipyhc** configuration parameters. Please note that
for some *Highcharts* features you might still need to write some Javascript,
often copy-pasted and tweaked from the official documentation.

### Dimensions

`width: int or str, default="100%", height: int, default=500`

The size of the grid is determined by the `width` and `height` parameters.
Width can either be a number of pixels or a string like `80%` or `550px` Otherwise the width will automatically be set to 100%, and height to 500px.

<<< @/content/code-snippets/dimensions.python

And the output:

<example-dimensions/>

### HighStock

`stock: bool, default=False`

The `stock` parameter is used to choose whether or not the graph should be an instance of `HighChart` or `HighStock`.

### Sync Chart

`unsync: bool, default=False`

If you want to export the chart selected data in `chart.data_out` as soon as the
Stock selection changes, set `unsync` to `False`.

See some examples of this synchonization in the [Examples](./examples.html) section.

<!--

::: tip
At any moment, if you want to know how to create an ipyaggrid like the doc examples,
search the example you need in this interactive notebook:

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/DGothrek%2Fipyaggrid/binder-demo?filepath=doc-builder.ipynb)
:::

::: tip Note
The Python-JavaScript conversion of list/dict-array/object is handled by
the ipywidgets library.
:::

## Parameters

This section describes the **ipyaggrid** basic configuration parameters - as
opposed to the [advanced customization parameters](./customize.html#params)
which often require JavaScript and CSS.

### Dimensions and placement

`width: int or string, default='100%'`  
`height: int, default=350`
`center: bool, default=False`

The size of the grid is determined by the `width` and `height` parameters.  
Width can either be a number of pixels or a string like `80%` or `550px`
Otherwise the width will automatically be set to `100%`, and height to `350px`.

The `center` param sets `margin` to `auto` for the `div` containing the grid and
the menu. It should be helpful if you wish to export the grid in other contexts
than the notebook.

<<< @/content/code-snippets/grid-size.python

<example-grid-size/>

## Live Demo

### Play with Data

In addition to manipulating the grid data using the frontend interface, a user
can also perform some operations from Python:

- `.export_grid()`: export all data
- `.export_selected_rows()`: export only selected rows
- `.export_selected_columns()`: export only selected columns
- `.update_grid_data(data)`: replace data in existing grid
- `.delete_selected_rows()`: delete only selected rows

### MyBinder

The following live notebook on [mybinder](https://mybinder.org) contains the 
examples of the previous sections:
[![Binder](https://mybinder.org/badge.svg)]
(https://mybinder.org/v2/gl/DGothrek%2Fipyaggrid/binder-demo?filepath=demo-ipyaggrid-python-functions.ipynb) -->
