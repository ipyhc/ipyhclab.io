module.exports = {
    title: 'ipyhc',
    description: 'Using Highcharts interactively in a Jupyter notebook',
    base: '/', // Comment if in dev mode
    dest: '../public',
    head: [['link', { rel: 'icon', href: '/favicon-16x16.png' }]],
    // serviceWorker: true,
     themeConfig: {
         algolia: {
             apiKey: 'b906b14ca6b79c840ca3ae0f241191bb',
             indexName: 'ipyhc'
         },
        
        repo: 'https://gitlab.com/ipyhc/ipyhc',
        editLinks: false,
        editLinkText: 'Edit this page on GitLab',
        lastUpdated: 'Last Updated',
        nav: [
            {
                text: 'Overview',
                link: '/overview/purpose',
            },
            {
                text: 'User Guide',
                link: '/guide/install',
            },
            {
                text: 'Development',
                link: '/dev/dev_install',
            },
        ],
        sidebarDepth: 5,
        sidebar: [
            {
                title: 'Overview',
                collapsable: false,
                children: ['/overview/purpose', '/overview/highcharts'],
            },
            {
                title: 'User Guide',
                collapsable: false,
                children: [
                    '/guide/install',
                    '/guide/create',
                    '/guide/examples',
                    '/guide/export',
                ],
            },
            {
                title: 'Developer',
                collapsable: false,
                children: [
                    '/dev/dev_install',
                    '/dev/publish',
                    '/dev/doc',
                    '/dev/structure',
                ],
            },
        ],
    },
    markdown: {
        lineNumbers: false,
    },
};
