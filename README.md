Static documentation website for ipyhc, deployed at [https://ipyhc.gitlab.io/](https://ipyhc.gitlab.io/).

The code is available at [https://gitlab.com/ipyhc/ipyhc](https://gitlab.com/ipyhc/ipyhc).